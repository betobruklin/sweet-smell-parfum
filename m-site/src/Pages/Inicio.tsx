import React from 'react';
import Return from '../Img/return.png';
import Truck from '../Img/truck.png';
import Lock from '../Img/lock.png';
import Produts from '../Img/produts.png';
import Img from '../Img/Img.jpg';
import azzaro from '../Img/azzaro.jpg';
import pacco from '../Img/1million.jpg';
import totsexy from '../Img/totsexy.jpg';
import dolce from '../Img/dolce.jpg';
import perfume from '../Img/beauty.svg';
import { Redirect } from 'react-router';


class Inicio extends React.Component {

    constructor(props: any) {
        super(props);


    }
    state = {

        redirect: false,

    }

    chamaForm = () => {
        this.setState({
            redirect: true
        })
    }



    render() {

        if (this.state.redirect) {
            return <Redirect to="/form" />

        }
        else {

            return (

                <div className='Inicio'>

                    {/* <div className="jumbotron-color text-center">
                        <img src={Img} width='100%' height='400px' alt='logo' /> */}
                    {/* <h1>Starship Technology</h1> */}

                    {/* </div> */}

                    <section className="apresentacao container-fluid">
                        <div className="row">
                            <div className="col-6 descricao">
                                <h1>Temos os melhores perfumes, venha conferir!</h1>
                                <p>Masculinos, Femininos e Kits...</p>
                                <button type="button" className="btn btn-outline-light">Ver todos</button>
                            </div>
                            <div className="col-6 text-white destaque">
                                {/* <i className="fas fa-mobile"></i> */}
                                <img src={perfume} alt=""/>
                            </div>
                        </div>
                        {/* <div className="ver-mais">
                            <i className="far fa-arrow-alt-circle-down"></i>
                        </div> */}
                    </section>



                    <div className="product container-fluid">

                        <div className="row">

                            <h1 className="section-title col-12">Produtos Mais Vendidos</h1>

                            <div className="card col-2 mx-auto">
                                <img className="card-img-top" src={azzaro} alt="Card image cap"></img>
                                <div className="card-body">
                                    <div className="card-text">
                                    <p><b>Perfume Azzaro Silver Black</b><br/> Masculino Eau de Toilette 100ml</p>
                                    <hr /><p>por: R$ 199,90</p>
                                    </div>
                                    <div className="card-footer">
                                        <button type="submit" onClick={() => this.chamaForm()}><i className="fas fa-shopping-cart"></i> Comprar</button>
                                    </div>
                                </div>
                            </div>


                            <div className="card col-2 mx-auto">
                                <img className="card-img-top" src={totsexy} alt="Card image cap"></img>
                                <div className="card-body">
                                    <div className="card-text">
                                    <p><b>Perfume 212 Sexy Carolina Herrera</b><br/> Feminino Eau de Parfum 60ml / 100ml</p>
                                    <hr />por: 399,00 / 499,00 <p/>
                                    <br/>
                                    </div>
                                    <div className="card-footer">
                                        <button type="submit" onClick={() => this.chamaForm()}><i className="fas fa-shopping-cart"></i> Comprar</button>
                                    </div>
                                </div>
                            </div>

                            <div className="card col-2 mx-auto">
                                <img className="card-img-top" src={pacco} alt="Card image cap"></img>
                                <div className="card-body">
                                    <div className="card-text">
                                    <p><b>Perfume Paco Rabanne 1 Million</b><br/> Masculino Eau de Toilette 100ml / 200ml</p>
                                    <hr /><p>por: R$ 479,00 / 549,00</p>
                                    </div>
                                    <div className="card-footer">
                                        <button type="submit" onClick={() => this.chamaForm()}><i className="fas fa-shopping-cart"></i> Comprar</button>
                                    </div>
                                </div>
                            </div>

                            <div className="card col-2 mx-auto">
                                <img className="card-img-top" src={dolce} alt="Card image cap"></img>
                                <div className="card-body">
                                    <div className="card-text">
                                    <p><b>Perfume Light Blue Dolce&Gabbana</b><br/> Feminino Eau de Toilette 50ml / 100ml</p>
                                    <hr />por: R$ 430,00 / 620,00
                                    </div>
                                    <div className="card-footer">
                                        <button type="submit" onClick={() => this.chamaForm()}><i className="fas fa-shopping-cart"></i> Comprar</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                        

                    </div>

                   
                </div>
            );
        }
    }
};

export default Inicio;