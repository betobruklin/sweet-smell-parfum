import React from 'react';
import { Link } from 'react-router-dom';

const Menu: React.FC = () => {
    return (
        <header>
            <nav className='Menu'>
                <ul className="ml-5">
                    <li><Link to='/'>INICIO</Link></li>
                    <li><Link to='/ListProd'>LISTA DE PRODUTOS</Link></li>
                    <li><Link to='/form'>DADOS DO CLIENTE</Link></li>
                    <li className="comprar"><Link to='/Checkout'><i className="fas fa-shopping-basket"></i></Link></li>
                </ul>
            </nav>

        </header>

    );
};

export default Menu;