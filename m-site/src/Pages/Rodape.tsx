import React from 'react';
import Face from '../Img/face.png';
import Tw from '../Img/twitter.png';
import {Link} from 'react-router-dom';

const Rodape: React.FC = () => {
  return (

    // <div>
    //   <footer className="site-footer">
    //     <div className="zigzag-bottom"></div>
    //     <div className="container">
    //       <div className="row">
    //           <div className="col-sm-12 col-md-6">
    //            <h6>About</h6>
    //           <p className="text-justify">Starship Technology </p>
    //         </div> 

    //         <div className="col-xs-6 col-md-3">
    //           <h6>Categorias</h6>
    //           <ul className="footer-links">
    //             <li><a>Celulares</a></li>
    //             <li><a>Notebook</a></li>
    //             <li><a>Televisão</a></li>
                
    //           </ul>
    //         </div>

    //         <div className="col-xs-6 col-md-3">
    //           <h6> Links</h6>
    //           <ul className="footer-links">
    //           <li><Link to='/'>INICIO</Link></li>
    //                <li><Link to='/ListProd'>LISTA DE PRODUTOS</Link></li>
    //                <li><Link to='/form'>DADOS DO CLIENTE</Link></li>
    //                <li><Link to='/Checkout'>Checkout</Link></li>
    //           </ul>
    //         </div>
    //       </div>
    //       <hr>
    //       </hr>
    //       <div className="container">
    //         <div className="row">
    //           <div className="col-md-8 col-sm-6 col-xs-12">
    //             <p className="copyright-text"> 2020 All Rights Reserved by &nbsp; 
    //      <a href="#">Starship Technology</a>
    //         </p>
    //           </div>

    //           <div className="col-md-4 col-sm-6 col-xs-12">
    //             <ul className="social-icons">
    //                <li><a className="facebook" href="#"><img src={Face} width='38px' height='38px' alt='logo'/></a></li>
    //           <li><a className="twitter" href="#"><img src={Tw} width='58px' height='58px' alt='logo'/></a></li>
    //             </ul>
    //           </div>
    //         </div>
    //        </div>
    //        </div>
    //   </footer>

    // </div>


    <div className="rodape">
      <footer className="footer text-center">
            <div className="container">
                <div className="row">
                    {/* <!-- Footer Location--> */}
                    <div className="col-lg-4 mb-5 mb-lg-0">
                        <h4 className="text-uppercase mb-4">Location</h4>
                        <p className="lead mb-0">2215 John Daniel Drive<br />Clark, MO 65243</p>
                    </div>
                    {/* <!-- Footer Social Icons--> */}
                    <div className="col-lg-4 mb-5 mb-lg-0">
                        <h4 className="text-uppercase mb-4">Around the Web</h4>
                        <a className="btn btn-outline-light btn-social mx-1" href="#!"><i className="fab fa-fw fa-facebook-f"></i></a><a className="btn btn-outline-light btn-social mx-1" href="#!"><i className="fab fa-fw fa-twitter"></i></a><a className="btn btn-outline-light btn-social mx-1" href="#!"><i className="fab fa-fw fa-linkedin-in"></i></a><a className="btn btn-outline-light btn-social mx-1" href="#!"><i className="fab fa-fw fa-dribbble"></i></a>
                    </div>
                    {/* <!-- Footer About Text--> */}
                    <div className="col-lg-4">
                        <h4 className="text-uppercase mb-4">Contact Us</h4>
                        <p className="lead mb-0"> Call: 271 895 512 31 <br/>
                        <a href="http://sweetsmellparfum@org.com">sweetsmellparfum.com</a></p>
                    </div>
                </div>
            </div>
        </footer>
        {/* <!-- Copyright Section--> */}
        <div className="copyright py-4 text-center text-white">
            <div className="container"><small>Copyright Sweet Smell Website 2020</small></div>
        </div>
    </div>

    
  );
};

export default Rodape;