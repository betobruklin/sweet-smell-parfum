import React from 'react';
import { Redirect } from 'react-router';

class form extends React.Component {
    constructor(props: any) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.alterarNome = this.alterarNome.bind(this);
        this.alterarSobrenome = this.alterarSobrenome.bind(this);
        this.alterarEmail = this.alterarEmail.bind(this);
        this.alterarCpf = this.alterarCpf.bind(this);
        this.alterarPag = this.alterarPag.bind(this);
        this.alterarCn = this.alterarCn.bind(this);
        this.alterarNC = this.alterarNC.bind(this);
        this.alterarDE = this.alterarDE.bind(this);


    }
    state = {
        nome: '',
        sobrenome: '',
        cpf: '',
        email: ' ',
        Pag: '',
        NCartao: '',
        CNcartao: '',
        DE: '',
        cvv: '',
        redirect: false,


        address: {

            cep: "",
            city: "",
            neighborhood: "",
            state: "",
            street: ""
        },
        cepInput: ""

    }

    onInputChange(event: any) {
        this.setState({
            cepInput: event.target.value
        })
    }

    alterarNome = (event: any) => {

        this.setState({ nome: event.target.value });
    }

    alterarSobrenome = (event: any) => {

        this.setState({ sobrenome: event.target.value });
    }


    alterarEmail = (event: any) => {
        this.setState({ email: event.target.value });
    }

    alterarCpf = (event: any) => {
        this.setState({ cpf: event.target.value });
    }




    alterarPag = (event: any) => {
        this.setState({ Pag: event.target.value });

    }

    alterarCn = (event: any) => {

        this.setState({ NCartao: event.target.value });
    }

    alterarNC = (event: any) => {

        this.setState({ CNartao: event.target.value });
    }

    alterarDE = (event: any) => {

        this.setState({ DE: event.target.value });
    }

    alterarCVV = (event: any) => {

        this.setState({ cvv: event.target.value });
    }


    handleSubmit(event: any) {
        fetch(`http://localhost:3001/cep?value=${this.state.cepInput}`)
            .then(
                res => res.json()
            ).then(addressFromApi => {
                this.setState({
                    address: addressFromApi
                })

                this.setState({
                    address: ({ city: this.state.address.city })
                })


            })

            .catch(error => {
                console.log(error);
            })
            ;

        event.preventDefault();
    }



    chamaForm = () => {
        this.setState({
            redirect: true
        })
    }



    render() {

        if (this.state.redirect) {
            return <Redirect to="/Checkout" />

        }
        else {

            return (
                <div className="container">
                    <div>
                        <h1 className="title-config">PREENCHA COM SEUS DADOS</h1>
                    </div>


                    <form onSubmit={this.handleSubmit}>

                        <div className="form-group">
                            <label>Nome: </label>
                            <input type='text' className='form-control' name='nome' placeholder=' Digite o seu nome!' required value={this.state.nome} onChange={this.alterarNome} />

                        </div>

                        <div className="form-group">
                            <label>Sobrenome: </label>
                            <input type='text' className='form-control' name='nome' placeholder=' Digite o seu sobrenome!' required value={this.state.sobrenome} onChange={this.alterarSobrenome} />

                        </div>


                        <div className="form-group">
                            <label>E-mail: </label>
                            <input type='email' className='form-control' name='email' placeholder='Por exemplo mauro@email.com' required value={this.state.email} onChange={this.alterarEmail} />
                        </div>


                        <div className="form-group">
                            <label>CPF: </label>
                            <input type='text' className='form-control' name='CPF' placeholder='Por exemplo: 000.000.000-00' required value={this.state.cpf} onChange={this.alterarCpf} />
                        </div>


                        {/* fazer endereço automaticamente */}
                        <div className="form-group">
                            <label>Cep: </label>
                            <input className='form-control' onChange={event => this.onInputChange(event)} />

                        </div>

                        <div className="form-group">
                            <label>Cidade: </label>
                            <input className='form-control' onChange={event => this.onInputChange(event)} />
                        </div>

                        <div className="form-group">
                            <label>Ponto de Referência: </label>
                            <input className='form-control' onChange={event => this.onInputChange(event)} />
                        </div>

                        <div className="form-group">
                            <label>Estado: </label>
                            <input className='form-control' onChange={event => this.onInputChange(event)} />
                        </div>

                        <div className="form-group">
                            <label>Rua: </label>
                            <input className='form-control' onChange={event => this.onInputChange(event)} />
                        </div>

                        {/* Pagamento */}

                        <h2 className="title-config">FORMA DE PAGAMENTO</h2>
                        
                        <div className="Pagamento form-group">
                            <label>Pagamento: </label>
             <select className='form-control' value={this.state.Pag} onChange={this.alterarPag} name="Pagamento" required>
                                <option value="">Selecione</option>
                                <option value="Cartão de credito">Cartão de crédito</option>
                                <option value="Cartão de debito">Cartão de débito</option>
                                <option value="PayPal">PayPal</option>
                            </select>
                        </div>

                        <div className="form-group">
                            <label>Nome do titular do cartão: </label>
                            <input type='text' className='form-control' name='NCartao' placeholder='Nome completo, como mostrado no cartão' required value={this.state.NCartao} onChange={this.alterarCn} />
                        </div>

                        <div className="form-group">
                            <label>Número do cartão: </label>
                            <input type='text' className='form-control' name='CNartao' placeholder='' required value={this.state.CNcartao} onChange={this.alterarCn} />
                        </div>

                        <div className="form-group">
                            <label>Data de validade do cartão: </label>
                            <input type='text' className='form-control' name='Cartao' placeholder='' required value={this.state.DE} onChange={this.alterarDE} />
                        </div>

                        <div className="form-group">
                            <label>CVV: </label>
                            <input type='text' className='form-control' name='Cartao' placeholder='' required value={this.state.nome} onChange={this.alterarNome} />
                        </div>
                        

                        <button type="submit" className="btn btn-success d-block mx-auto p-2 m-5"  onClick={() => this.chamaForm()}> Confirmar compra</button>

                    </form>

                </div >
            )

        }

    };
}

export default form;
