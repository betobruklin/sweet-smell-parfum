import React from 'react';
import { Redirect } from 'react-router';

import azzaro from '../Img/azzaro.jpg';
import pacco from '../Img/1million.jpg';
import polo from '../Img/polo.jpg';
import tot from '../Img/tot.jpg';

import dolce from '../Img/dolce.jpg';
import dior from '../Img/dior.jpg';
import lancome from '../Img/lancome.jpg';
import totsexy from '../Img/totsexy.jpg';


import kpr from '../Img/kpr.jpg';
import kab from '../Img/kab.jpg';
import ktotvr from '../Img/ktotvr.jpg';
import nina from '../Img/nina.jpg';

class Inicio extends React.Component {

    constructor(props: any) {
        super(props);


    }
    state = {

        redirect: false,

    }

    chamaForm = () => {
        this.setState({
            redirect: true
        })
    }



    render() {

        if (this.state.redirect) {
            return <Redirect to="/form" />

        }
        else {

            return (

                <div className="product container-fluid">
                    <div className="row">

                        <h1 className="section-title col-12">Todos os produtos</h1>
                        <div className="row">

                        <div className="card col-2 mx-auto mb-3">
                            <img className="card-img-top" src={azzaro} alt="Card image cap"></img>
                            <div className="card-body">
                                <div className="card-text">
                                    <p><b>Perfume Azzaro Silver Black</b></p>
                                    <hr />
                                    <span>Masculino Eau de Toilette 100ml</span>
                                    <p>por: R$ 199,90</p>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" onClick={() => this.chamaForm()}>
                                        <i className="fas fa-shopping-cart text-right"></i> Adicionar</button>
                                </div>
                            </div>
                        </div>


                        <div className="card col-2 mx-auto mb-3">
                            <img className="card-img-top" src={pacco} alt="Card image cap"></img>
                            <div className="card-body">
                                <div className="card-text">
                                <p><b>Perfume Paco Rabanne 1 Million </b><br /> Masculino Eau de Toilette<br /> 100ml / 200ml</p>
                                    <p>por: R$ 479,90 / 549,90</p>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" onClick={() => this.chamaForm()}>
                                        <i className="fas fa-shopping-cart text-right "></i> Carrinho</button>
                                </div>
                            </div>
                        </div>


                        <div className="card col-2 mx-auto mb-3">
                            <img className="card-img-top" src={tot} alt="Card image cap"></img>
                            <div className="card-body">
                                <div className="card-text">
                                <p><b>Perfume Carolina Herrera 212 </b><br /> Masculino Eau de Toilette<br /> 100ml / 200ml</p>
                         <p>por: R$ 469,00 / 519,00</p>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" onClick={() => this.chamaForm()}>Comprar</button>
                                </div>
                            </div>
                        </div>


                        <div className="card col-2 mx-auto mb-3">
                            <img className="card-img-top" src={polo} alt="Card image cap"></img>
                            <div className="card-body">
                                <div className="card-text">
                                <p><b>Polo Blue </b><br /> Masculino Eau de Toilette 125ml / 200ml</p>
                                    <p>por: R$ 499,00 /589,00</p>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" onClick={() => this.chamaForm()}>Comprar</button>
                                </div>
                            </div>
                        </div>
                        </div>

                        <div className="row">

                        <div className="card col-2 mx-auto mb-3">
                            <img className="card-img-top" src={dolce} alt="Card image cap"></img>
                            <div className="card-body">
                                <div className="card-text">
                                <p><b>Perfume Light Blue Dolce&Gabbana </b><br /> Feminino Eau de Toilette<br />  50ml / 100ml</p>        
                                    <p>por: R$ 430,00 / 620,00</p>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" onClick={() => this.chamaForm()}>Comprar</button>
                                </div>
                            </div>
                        </div>


                        <div className="card col-2 mx-auto mb-3">
                            <img className="card-img-top" src={dior} alt="Card image cap"></img>
                            <div className="card-body">
                                <div className="card-text">
                                <p><b>Perfume J'adore Dior </b><br /> Feminino Eau de Parfum 50ml / 100ml </p>
                         <p>por: R$ 459,00 / 599,00</p>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" onClick={() => this.chamaForm()}>Comprar</button>
                                </div>
                            </div>
                        </div>


                        <div className="card col-2 mx-auto mb-3">
                            <img className="card-img-top" src={lancome} alt="Card image cap"></img>
                            <div className="card-body">
                                <div className="card-text">
                                <p><b>Perfume Lancôme La Vie est Belle </b><br /> Feminino L'Eau de Parfum<br /> 50ml / 75ml / 100ml </p>
                         <p>por: 439,00 / 529,00 / 579,00</p>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" onClick={() => this.chamaForm()}>Comprar</button>
                                </div>
                            </div>
                        </div>



                        <div className="card col-2 mx-auto mb-3">
                            <img className="card-img-top" src={totsexy} alt="Card image cap"></img>
                            <div className="card-body">
                                <div className="card-text">
                                <p><b>Perfume 212 Sexy Carolina Herrera </b><br /> Feminino Eau de Parfum<br /> 60ml / 100ml </p>
                         <p>por: 399,00 / 499,00</p>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" onClick={() => this.chamaForm()}>Comprar</button>
                                </div>
                            </div>
                        </div>
                        </div>


                    <div className="row">
                        <div className="card col-2 mx-auto mb-3">
                            <img className="card-img-top" src={kpr} alt="Card image cap"></img>
                            <div className="card-body">
                                <div className="card-text">
                                <p><b>Kit Coffret Paco Rabanne 1 Million </b><br /> Masculino Eau de Toilette<br /> (Full Size 100ml + Travel Size 10ml)</p>
                         <p>por: R$ 479,00</p>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" onClick={() => this.chamaForm()}>Comprar</button>
                                </div>
                            </div>
                        </div>



                        <div className="card col-2 mx-auto mb-3">
                            <img className="card-img-top" src={kab} alt="Card image cap"></img>
                            <div className="card-body">
                                <div className="card-text">
                                <p><b>Kit Coffret Paco Rabanne 1 Million </b><br /> Masculino Eau de Toilette<br /> (Full Size 100ml + Travel Size 10ml)</p>
                         <p>por: R$ 169,00</p>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" onClick={() => this.chamaForm()}>Comprar</button>
                                </div>
                            </div>
                        </div>




                        <div className="card col-2 mx-auto mb-3">
                            <img className="card-img-top" src={ktotvr} alt="Card image cap"></img>
                            <div className="card-body">
                                <div className="card-text">
                                <p><b>Kit Coffret Carolina Herrera 212 Vip Rosé </b><br /> Feminino Eau de Parfum<br /> (1 Full Size Perfume Carolina Herrera 212 Vip Rosé Feminino Eau de Parfum 80 ml + <br /> 1 Body Lotion Carolina Herrera 212 Vip Rosé 100 ml)</p>
                         <p>por: R$ 509,00</p>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" onClick={() => this.chamaForm()}>Comprar</button>
                                </div>
                            </div>
                        </div>




                        <div className="card col-2 mx-auto mb-3">
                            <img className="card-img-top" src={nina} alt="Card image cap"></img>
                            <div className="card-body">
                                <div className="card-text">
                                <p><b>Kit Coffret Nina Ricci Bella Feminino Eau de Toilette</b><br /> (1 Perfume Feminino Bella Nina Ricci Eau de Toilette - 80ml + <br />
// 1 Loção Corporal - 100ml)</p>
                         <p>por: R$ 429,00</p>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" onClick={() => this.chamaForm()}>Comprar</button>
                                </div>
                            </div>
                        </div>

                        </div>




                    </div>
                </div>



//                 <div className='ListaProdutos'>

//                     <div className="Logo">
//                         <h1>LISTA DE PRODUTOS</h1>
//                     </div>

//                     <hr />
//                     <div className='title-1' >

//                         <h4>PERFUMES MASCULINOS</h4>
//                     </div>
//                     <hr />
//                     <div className='Azzaro'>
//                         <p></p>
//                         <img src={azzaro} width='158px' height='158px' alt='logo' />
//                         <p><b>Perfume Azzaro Silver Black </b><br /> Masculino Eau de Toilette <br /> 100ml </p>
//                         <p>por: R$ 199,90</p>
//                         <input type="submit" value="COMPRAR" onClick={() => this.chamaForm()} />
//                     </div>
//                     <p />
//                     <div className='Pacco'>
//                         <img src={pacco} width='158px' height='168px' alt='logo' />
//                         <p><b>Perfume Paco Rabanne 1 Million </b><br /> Masculino Eau de Toilette<br /> 100ml / 200ml</p>
//                         <p>por: R$ 479,90 / 549,90</p>
//                         <input type="submit" value="COMPRAR" onClick={() => this.chamaForm()} />

//                     </div>
//                     <p />
//                     <div className='Tot'>

//                         <img src={tot} width='158px' height='170px' alt='logo' />
//                         <p><b>Perfume Carolina Herrera 212 </b><br /> Masculino Eau de Toilette<br /> 100ml / 200ml</p>
//                         <p>por: R$ 469,00 / 519,00</p>
//                         <input type="submit" value="COMPRAR" onClick={() => this.chamaForm()} />

//                     </div>
//                     <p />

//                     <div className='Polo'>

//                         <img src={polo} width='158px' height='188px' alt='logo' />
//                         <p><b>Polo Blue </b><br /> Masculino Eau de Toilette <br /> 125ml / 200ml</p>
//                         <p>por: R$ 499,00 / 589,00</p>
//                         <input type="submit" value="COMPRAR" onClick={() => this.chamaForm()} />

//                     </div>
//                     <hr />

//                     <div className='title-2'>
//                         <h4>PERFUMES FEMININOS</h4>
//                     </div>
//                     <hr />

//                     <div className='Dolce'>

//                         <img src={dolce} width='158px' height='168px' alt='logo' />
//                         <p><b>Perfume Light Blue Dolce&Gabbana </b><br /> Feminino Eau de Toilette<br />  50ml / 100ml</p>
//                         <p>por: R$ 430,00 / 620,00</p>
//                         <input type="submit" value="COMPRAR" onClick={() => this.chamaForm()} />

//                     </div>
//                     <p />
//                     <div className='Dior'>

//                         <img src={dior} width='158px' height='168px' alt='logo' />
//                         <p><b>Perfume J'adore Dior </b><br /> Feminino Eau de Parfum<br /> 50ml / 100ml </p>
//                         <p>por: R$ 459,00 / 599,00</p>
//                         <input type="submit" value="COMPRAR" onClick={() => this.chamaForm()} />

//                     </div>
//                     <p />
//                     <div className='Lancome'>

//                         <img src={lancome} width='158px' height='168px' alt='logo' />
//                         <p><b>Perfume Lancôme La Vie est Belle </b><br /> Feminino L'Eau de Parfum<br /> 50ml / 75ml / 100ml </p>
//                         <p>por: 439,00 / 529,00 / 579,00</p>
//                         <input type="submit" value="COMPRAR" onClick={() => this.chamaForm()} />

//                     </div>
//                     <p />
//                     <div className='totsexy'>

//                         <img src={totsexy} width='158px' height='168px' alt='logo' />
//                         <p><b>Perfume 212 Sexy Carolina Herrera</b><br /> Feminino Eau de Parfum<br /> 60ml / 100ml </p>
//                         <p>por: 399,00 / 499,00</p>
//                         <input type="submit" value="COMPRAR" onClick={() => this.chamaForm()} />

//                     </div>

//                     <hr />
//                     <div className='title-3'>

//                         <h4> KITS </h4>

//                     </div>

//                     <hr />
//                     <div className='Kpr'>

//                         <img src={kpr} width='158px' height='168px' alt='logo' />
//                         <p><b>Kit Coffret Paco Rabanne 1 Million </b><br /> Masculino Eau de Toilette<br /> (Full Size 100ml + Travel Size 10ml)</p>
//                         <p>por: R$ 479,00</p>
//                         <input type="submit" value="COMPRAR" onClick={() => this.chamaForm()} />

//                     </div>

//                     <div className='Kab'>

//                         <img src={kab} width='158px' height='168px' alt='logo' />
//                         <p><b>Kit Coffret Antonio Banderas The Golden Secret </b><br /> Masculino Eau de Toilette<br /> (1 Perfume Masculino The Golden Secret Antonio Banderas - 100ml + <br />1 Balm Pós Barba The Golden Secret Antonio Banderas - 75ml)</p>
//                         <p>por: R$ 169,00</p>
//                         <input type="submit" value="COMPRAR" onClick={() => this.chamaForm()} />

//                     </div>

//                     <div className='Ktotvr'>

//                         <img src={ktotvr} width='158px' height='168px' alt='logo' />
//                         <p><b>Kit Coffret Carolina Herrera 212 Vip Rosé </b><br /> Feminino Eau de Parfum<br /> (1 Full Size Perfume Carolina Herrera 212 Vip Rosé Feminino Eau de Parfum 80 ml + <br /> 1 Body Lotion Carolina Herrera 212 Vip Rosé 100 ml)</p>
//                         <p>por: R$ 509,00</p>
//                         <input type="submit" value="COMPRAR" onClick={() => this.chamaForm()} />

//                     </div>

//                     <div className='Nina'>

//                         <img src={nina} width='158px' height='168px' alt='logo' />
//                         <p><b>Kit Coffret Nina Ricci Bella Feminino Eau de Toilette</b><br /> (1 Perfume Feminino Bella Nina Ricci Eau de Toilette - 80ml + <br />
// 1 Loção Corporal - 100ml)</p>
//                         <p>por R$ 429,90</p>
//                         <input type="submit" value="COMPRAR" onClick={() => this.chamaForm()} />
//                         <p />
//                     </div>



//                 </div>
            );
        }
    }
};

export default Inicio;