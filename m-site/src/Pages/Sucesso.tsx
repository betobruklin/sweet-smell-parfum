import React from 'react';
import { Redirect } from 'react-router';

class Succ extends React.Component {
    constructor(props: any) {
        super(props);
    }
    state = {

        redirect: false,

    }


    chamaForm = () => {
        this.setState({
            redirect: true
        })
    }


    render() {
        if (this.state.redirect) {
            return <Redirect to="/" />

        }
        else {
            return (

                     
                <div>
                    
                <div className='msg'>

                    <h2> SUA COMPRA FOI REALIZADA COM SUCESSO</h2>
                    <p>Obrigado por utilizar os nossos serviços</p>
                    <input type="submit" value="OK" onClick={() => this.chamaForm()} />

                </div>
                
                </div>

            );
        }
    }
};

export default Succ;