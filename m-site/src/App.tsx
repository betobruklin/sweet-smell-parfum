import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Menu from './Pages/Menu';
import Rodape from './Pages/Rodape';
import Inicio from './Pages/Inicio';
import Formulario from './Pages/Formulario';
import Checkout from './Pages/Checkout';
import Lista from './Pages/ListProd';



function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Menu />
        <Switch>
        
          <Route exact path="/" component={Inicio} />
          <Route path="/ListProd" component={Lista} />
          <Route path="/form" component={Formulario} />
          <Route path="/checkout" component={Checkout} />
          
           
        </Switch>
       <Rodape />
      </BrowserRouter>

    </div>
  );
}

export default App;
